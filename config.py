SECRET_KEY = 'b2d6306ada22f040fe777daa9e56fa4fcebaf0aed3318a62'
DEBUG = False
# format: <ip or hostname> or <ip or hostname:port>
SERVER_NAME = '127.0.0.1'
SQLALCHEMY_DATABASE_URI = 'sqlite:///../timetrack.db'
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
JSON_SORT_KEYS = False
JSONIFY_PRETTYPRINT_REGULAR = DEBUG
SESSION_COOKIE_HTTPONLY = False
SESSION_COOKIE_SECURE = True
# options: strong, basic, false
SESSION_PROTECTION = 'strong'