'use strict';

// Declare app level module which depends on views, and components
angular.module('timetrack', [
  'ngRoute',
  'timetrack.login',
  'timetrack.register'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .otherwise({redirectTo: '/'});
}]);

