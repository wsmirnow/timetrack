from distutils.core import setup

setup(
    name='timetrack',
    version='0.1',
    packages=['timetrack', 'timetrack.auth', 'timetrack.model'],
    url='',
    license='LGPL',
    author='Waldemar Smirnow',
    author_email='waldemar.smirnow@gmail.com',
    description='TimeTrack is a web app to track your time. '
                'It should help you to remember, what you have done and haw many time you spend for it.'

)
