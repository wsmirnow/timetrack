from flask import Blueprint, request
from flask.ext.login import LoginManager, login_user, logout_user

from timetrack.model.models import User, db
from timetrack.shared import create_json_response

auth_app = Blueprint('auth', __name__)
login_manager = LoginManager()


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@auth_app.route('/register', methods=['POST'])
def register():
    username = request.form.get('username', None)
    password = request.form.get('password', None)
    email = request.form.get('email', None)
    if username is None or password is None or email is None:
        return create_json_response(400, 'User name, password or email not set.')
    user = User(username, password, email)
    db.session.add(user)
    try:
        db.session.commit()
    except:
        return create_json_response(400,
                                    message="Username exists, please choose an other.",
                                    json={'user': user.as_json()})

    return create_json_response(message='Welcome %s.' % (user.username, ),
                                json={'user': user.as_json()})


@auth_app.route('/login', methods=['POST'])
def login():
    username = request.form.get('username', None)
    password = request.form.get('password', None)
    remember = request.form.get('remember', False)
    registered_user = User.query.filter_by(username=username).first()
    if registered_user is None or not registered_user.check_password(password):
        # TODO fill WWW-Authenticate header
        return create_json_response(401, 'Username or password is invalid.')
    login_user(registered_user, remember=remember)
    return create_json_response(message='You are logged in.',
                                json={'user': registered_user.as_json()})


@auth_app.route('/logout', methods=['POST'])
def logout():
    logout_user()
    return create_json_response(message='You are logged out.')


def init_login_manager(app):
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'
