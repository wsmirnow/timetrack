from flask import Flask
from os import path


def create_app(config_file=None):
    app = Flask(__name__)
    default_config = path.abspath(path.join(path.dirname(__file__), '..', 'config.py'))
    app.config.from_pyfile(config_file or default_config)
    return app


def init_app(app):
    from timetrack.model.models import init_db
    init_db(app)

    from timetrack.auth.app import auth_app, init_login_manager
    init_login_manager(app)
    app.register_blueprint(auth_app)

    return app


if __name__ == '__main__':
    app = create_app()
    init_app(app)
    print app.url_map
    app.run()