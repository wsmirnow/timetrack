from flask import jsonify


def create_json_response(status_code=200, message=None, json={}):
    if json is None:
        json = dict()
    if message is not None:
        json.update({'message': message})
    return jsonify(json), status_code
